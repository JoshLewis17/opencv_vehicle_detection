from imutils.video import VideoStream
from imutils.video import FPS
import argparse
import imutils
import time
import cv2
import numpy as np
import argparse
import cv2
import json
from kafka import KafkaConsumer;

ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", type=str, help="path to input video file")
args = vars(ap.parse_args())
vs = cv2.VideoCapture(args["video"])

consumer = KafkaConsumer("valoresTrafico",group_id="group1", bootstrap_servers=['localhost:9092'])

frameCount = 0
while True:
	frame = vs.read()[1]
	frameCount += 1
	(h, w) = frame.shape[:2]
	detections = []
	
	for contador in consumer:
		frame = vs.read()[1]
		frameCount += 1
		(h, w) = frame.shape[:2]
		cv2.putText(frame, str(contador.value.decode("utf-8")), (25, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 2)
		cv2.imshow("Frame", frame)
		key = cv2.waitKey(23)

vs.release()
cv2.destroyAllWindows()