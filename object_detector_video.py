from imutils.video import VideoStream
from imutils.video import FPS
import argparse
import imutils
import time
import cv2
import numpy as np
import argparse
import cv2
import json
from kafka import KafkaProducer

ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", type=str)
ap.add_argument("-c", "--confidence", type=float, default=0.1)
ap.add_argument("-m", "--model", required=True)
ap.add_argument("-p", "--prototxt", required=True)
args = vars(ap.parse_args())

classes = ["fondo", "avion", "bici", "pajaro", "barco",
	"botella", "autobus", "coche", "gato", "silla", "vaca", "table",
	"perro", "caballo", "moto", "persona", "maceta", "oveja",
	"sofa", "tren", "tv"]
colors = np.random.uniform(0, 255, size=(len(classes), 3))
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])
vs = cv2.VideoCapture(args["video"])
# producer = KafkaProducer(bootstrap_servers=['localhost:9092'], value_serializer=lambda m: json.dumps(m).encode('ascii'))
while True:
	frame = vs.read()[1]
	if frame is None:
		break
	(h, w) = frame.shape[:2]
	blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)
	net.setInput(blob)
	detections = net.forward()
	detectionsJson = []
	for i in np.arange(0, detections.shape[2]):
		confidence = detections[0, 0, i, 2]
		if confidence > args["confidence"]:
			box = detections[0, 0, i, 3:7] * np.array([300, 300, 300, 300])
			(x1, y1, x2, y2) = box.astype("int")
			detectionJson = {
				'camera_id': 2,
				'class': classes[int(detections[0, 0, i, 1])],
				'confidence': float(confidence * 100),
				'box': {
					'startY': int(y1),
					'startX': int(x1),
					'endY': int(y2),
					'endX': int(x2),
				}
			}
			detectionsJson.append(detectionJson)
			
			idx = int(detections[0, 0, i, 1])
			box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")
			label = "{}: {:.2f}%".format(classes[idx], confidence * 100)
			print(str(label))
			cv2.rectangle(frame, (startX, startY), (endX, endY), (255,255,255), 2)
			y = startY - 15 if startY - 15 > 15 else startY + 15
			cv2.putText(frame, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 2)

	# producer.send('test', {'detections': detectionsJson})

	cv2.imshow("Frame", frame)
	key = cv2.waitKey(23)

vs.release()
cv2.destroyAllWindows()
