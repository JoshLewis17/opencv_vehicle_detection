from imutils.video import VideoStream
from imutils.video import FPS
import argparse
import imutils
import time
import cv2
import numpy as np
import argparse
import cv2
import json
from kafka import KafkaProducer;

ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", type=str, help="path to input video file")
ap.add_argument("-d", "--detections", type=str, help="path to json file")
args = vars(ap.parse_args())
vs = cv2.VideoCapture(args["video"])
producer = KafkaProducer(bootstrap_servers=['localhost:9092'], value_serializer=lambda m: json.dumps(m).encode('ascii'))

file = open(args["detections"], "r")
lines = file.readlines()
frameCount = 0
contador = 0;
while True:
	frame = vs.read()[1]
	if frame is None:
		break
	frameCount += 1
	(h, w) = frame.shape[:2]
	detections = []
	
	for detection in json.loads(lines[frameCount])['detections']:
		label = "{}: {:.2f}%".format(detection['class'], detection['confidence'])
		print(str(label))
		box = detection['box']
		d = {
			'camera_id': detection['camera_id'],
			'class': detection['class'],
			'confidence': detection['confidence'],
			'box': detection['box']
		}
		detections.append(d)
	print('contador: ' + str(contador))
	contador += 1
	producer.send('detectados',detections)
