class Box:

    startY: int
    startX: int
    endY: int
    endX: int

    def __init__(self,startX,startY,endY,endX):
        self.startX = startX
        self.startY = startY
        self.endX = endX
        self.endY = endY

    def calcularCentroide(self):
        cX = int((self.startX + self.endX) / 2)
        cY = int((self.startY + self.endY) / 2)
        return (cX,cY)


class Deteccion:

    camera_id: int
    clase: str
    confidence: float
    startX: int
    startY: int
    endX: int
    endY: int

    def __init__(self,camera_id,clase,confidence,box):
        self.camera_id = camera_id
        self.clase = clase
        self.confidence = confidence
        self.startX = box['startX']
        self.startY = box['startY']
        self.endX = box['endX']
        self.endY = box['endY']


class ObjetoDetectado:

    id: int
    clase: str
    centroidePrevio: int(2)
    centroideActual: int(2)
    contado: bool
    centroides = []
    desaparecido: int
    sentido: str
    deteccion: Deteccion
    box: Box
    parado: bool




















