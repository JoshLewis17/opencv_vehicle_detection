import pystorm
from collections import OrderedDict
from clases import ObjetoDetectado, Box, Deteccion
from scipy.spatial import distance
import numpy as np

class contador(pystorm.Bolt):

    idObjetos = 0
    objetosPerdidos: []
    totalesDerecha = 0
    totalesIzquierda = 0
    objetosDetectados = OrderedDict()
    detections = list()
    noDetecciones = 0
    maxDistancia = 0



    def initialize(self, stormconf, context):
        self._conf = stormconf
        self._context = context


    def process(self, tuple):


        self.detections = tuple.values[0]['detections']

        if(len(self.detections) == 0):
            self.noDetecciones += 1
        else:
            self.noDetecciones = 0

        if(self.noDetecciones >= 5):
            self.objetosDetectados.clear()
            self.noDetecciones = 0

        if(len(self.objetosDetectados.values()) == 0):

            for deteccion in self.detections:
                self.addObjeto(deteccion)

        elif(len(self.detections) > 0):
            centroidesEntrantes = list()
            nuevos = self.detections

            for nuevo in nuevos:
                CX = (nuevo['box']['startX'] + nuevo['box']['endX'])/2
                CY = (nuevo['box']['startY'] + nuevo['box']['endY'])/2
                centroide = [CX,CY]
                centroidesEntrantes.append(centroide)

            listaObjetosDetectados = list(self.objetosDetectados.keys())
            centroidesExistentes = list()
            asignados = list()

            for x in self.objetosDetectados.values():
                centroidesExistentes.append(x.centroideActual)

            distancias = distance.cdist(np.array(centroidesExistentes),centroidesEntrantes)
            distanciasF = distancias.min(axis=1).argsort()
            filasEmpleadas = list()
            columnasEmpleadas = list()
            distanciasC = distancias.argmin(axis=1)[distanciasF]
            clavesUsadas = list()

            for (f,c) in zip(distanciasF, distanciasC):
                if(f in filasEmpleadas or c in columnasEmpleadas):
                    continue

                buscado = self.objetosDetectados.get(listaObjetosDetectados[f])

                if(buscado.centroidePrevio == centroidesEntrantes[c]):
                    buscado.parado = True
                    buscado.centroides.clear()
                else:
                    buscado.parado = False

                if (distancias[f, c] > 200):
                    del buscado
                    asignados.append(nuevos[c])
                    filasEmpleadas.append(f)
                    columnasEmpleadas.append(c)
                    continue

                buscado.centroidePrevio = buscado.centroideActual
                buscado.centroides.append(buscado.centroidePrevio)
                buscado.centroideActual = centroidesEntrantes[c]
                buscado.desaparecidos = 0
                filasEmpleadas.append(f)
                columnasEmpleadas.append(c)
                asignados.append(nuevos[c])
                clavesUsadas.append(buscado.id)
                self.objetosDetectados[listaObjetosDetectados[f]] = buscado

            if (len(nuevos) > len(asignados)):
                for nuevo in nuevos:
                    if (nuevo not in asignados):
                        self.addObjeto(nuevo)

            elif(len(clavesUsadas) < len(self.objetosDetectados.keys())):
                for clave in self.objetosDetectados.keys():
                    if(clave not in clavesUsadas):
                        self.objetosDetectados[clave].desaparecido += 1

            self.eliminarDesaparecidos()

        salientes = 0
        for o in self.objetosDetectados.values():
            if(o.contado == False):

                if((salientes > len(self.detections)) or ( salientes > 2 ) ):
                    continue

                if(len(o.centroides) < 9):
                    continue

                if(o.parado == True):
                    continue

                media = 0
                for c in o.centroides:
                    media += c[0]
                media = media//len(o.centroides)

                sentido = o.centroideActual[0] - media
                sentidoAnterior = o.centroidePrevio[0] - media

                if(sentido * sentidoAnterior < 0 ):
                    continue

                if(sentido > 0):

                    if(o.centroideActual[0] > 265 and o.centroides[0][0] < o.centroideActual[0]):

                        if (o.clase in ['coche', 'moto', 'bici', 'autobus']):
                            self.totalesIzquierda += 1

                        o.contado = True
                        self.objetosDetectados[o.id] = o
                        salientes += 1
                if(sentido < 0):

                    if( o.centroideActual[0] < 35 and o.centroides[0][0] > o.centroideActual[0]):

                        if (o.clase in ['coche', 'moto', 'bici', 'autobus']):
                            self.totalesDerecha += 1

                        o.contado = True
                        self.objetosDetectados[o.id] = o
                        salientes += 1
            else:
                o.desaparecido += 1


        self.maxDistancia = 0

        self.detections = []

        totales = list()
        totales.append(self.totalesIzquierda)
        totales.append(self.totalesDerecha)

        self.emit([self.totalesIzquierda,self.totalesDerecha])



    def addObjeto(self, detection):

        newBox = detection['box']

        startX = newBox['startX']
        endX = newBox['endX']
        startY = newBox['startY']
        endY = newBox['endY']

        CX = (startX + endX)/2
        CY = (startY + endY)/2
        nuevoObjeto = ObjetoDetectado()
        nuevoObjeto.id = self.idObjetos
        nuevoObjeto.contado = False
        nuevoObjeto.clase = detection['clase']
        nuevoObjeto.desaparecido = 0
        nuevoObjeto.parado = False
        nuevoObjeto.centroidePrevio = (CX,CY)
        nuevoObjeto.centroideActual = nuevoObjeto.centroidePrevio
        nuevoObjeto.deteccion =  Deteccion(detection['camera_id'],detection['clase'],detection['confidence'],newBox)
        nuevoObjeto.box = Box(startX,startY,endX,endY)
        self.objetosDetectados[nuevoObjeto.id] = nuevoObjeto
        self.idObjetos = self.idObjetos + 1

    def eliminarDesaparecidos(self):

        clavesDesaparecidos = list()
        for clave in self.objetosDetectados.keys():
            if (self.objetosDetectados[clave].desaparecido > 10):
                clavesDesaparecidos.append(clave)

        for d in clavesDesaparecidos:
            objeto = self.objetosDetectados.get(d)
            del objeto



contador().run()