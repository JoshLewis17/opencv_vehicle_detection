/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controltrafico;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author benja
 */
public class Detections implements Serializable{
    
    @JsonProperty("detections")
    private List<Object> detections;

    public List<Object> getDetections() {
        return detections;
    }

    public void setDetections(List<Object> detections) {
        this.detections = detections;
    }

    @Override
    public String toString() {
        return '{' + "\"detections\": " + detections + '}';
    }

}
