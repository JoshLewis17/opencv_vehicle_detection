/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controltrafico;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.kafka.spout.KafkaSpoutConfig;
import org.apache.storm.topology.TopologyBuilder;

/**
 *
 * @author benja
 */
public class Main {
    
    public static void main(String[] args){
        
        KafkaSpoutConfig<String, String> kafkaSpoutConfig = KafkaSpoutConfig.builder("localhost:9092", "detectados")
                                                                      .setProp(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"none")
                                                                      .setProp(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,"com.mycompany.controltrafico.DeteccionesDeserializer")
                                                                      .setProp(ConsumerConfig.GROUP_ID_CONFIG,"groupId")
                                                                      .build();
        
        KafkaSpout kafkaSpout = new KafkaSpout(kafkaSpoutConfig);
        
        TopologyBuilder topologyBuilder = new TopologyBuilder();
        topologyBuilder.setSpout("kafkaSpout", kafkaSpout);
        topologyBuilder.setBolt("processBolt", new ProcessBolt()).shuffleGrouping("kafkaSpout");
        topologyBuilder.setBolt("contador", new Contador()).shuffleGrouping("processBolt");
        topologyBuilder.setBolt("printBolt", new PublisherBolt()).shuffleGrouping("contador");
        
        LocalCluster localCluster = new LocalCluster();
        
        Config config = new Config();
        config.setDebug(false);
        
        localCluster.submitTopology("controlTrafficTopology", config, topologyBuilder.createTopology());
        
    }
}
