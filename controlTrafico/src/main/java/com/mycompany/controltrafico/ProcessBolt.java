/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controltrafico;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

/**
 *
 * @author benja
 */
public class ProcessBolt implements IRichBolt{

    private OutputCollector collector;
    
    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void execute(Tuple tuple) {
        
        Deteccion[] detecciones = (Deteccion[]) tuple.getValues().get(4);
        List<Object> deteccionesImagen = new LinkedList();
        List<Object> detections = new LinkedList<>();
        for(Deteccion deteccion : detecciones){
            deteccionesImagen.add(deteccion);
        }
        
        Detections coleccionDetecciones = new Detections();
        coleccionDetecciones.setDetections(deteccionesImagen);
        this.collector.ack(tuple);
        
        this.collector.emit(new Values(coleccionDetecciones));
    }

    @Override
    public void cleanup() {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare( new Fields("detecciones"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
    
}
