/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controltrafico;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.kafka.common.serialization.Deserializer;

/**
 *
 * @author benja
 */
public class DeteccionesDeserializer implements Deserializer<Deteccion[]>{

    @Override
    public void configure(Map<String, ?> map, boolean bln) {
    }

    @Override
    public Deteccion[] deserialize(String string, byte[] bytes) {
        
        ObjectMapper objectMapper = new ObjectMapper();
        
        Deteccion[] detecciones = null;
        try{
            detecciones = objectMapper.readValue(bytes, Deteccion[].class);
        } catch (IOException ex) {
            Logger.getLogger(DeteccionesDeserializer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return detecciones;
    }

    @Override
    public void close() {
    }
    
}
