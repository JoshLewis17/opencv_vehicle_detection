/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controltrafico;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author benja
 */
public class Deteccion implements Serializable{
    
    private int camera_id;
    @JsonProperty("class")
    private String clase;
    private double confidence;
    private Box box;

    public Deteccion() {
    }

    public int getCamera_id() {
        return camera_id;
    }

    public void setCamera_id(int camera_id) {
        this.camera_id = camera_id;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    @Override
    public String toString() {
        return '{' + "\"camera_id\": " + camera_id + ", \"clase\": " + "\"" + clase + "\"" + ", \"confidence\": " + confidence + ", \"box\": " + box + '}';
    }
    
}
