/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controltrafico;

import java.io.Serializable;

/**
 *
 * @author benja
 */
public class Box implements Serializable{
    
    private int startX;
    private int startY;
    private int endX;
    private int endY;

    public int getStartX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public int getEndX() {
        return endX;
    }

    public void setEndX(int endX) {
        this.endX = endX;
    }

    public int getEndY() {
        return endY;
    }

    public void setEndY(int endY) {
        this.endY = endY;
    }

    @Override
    public String toString() {
        return '{' + "\"startY\": " + startY + ", \"startX\": " + startX + ", \"endY\": " + endY + ", \"endX\": " + endX + '}';
    }
    
}
